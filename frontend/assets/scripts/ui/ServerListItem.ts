
import { _decorator, Component, Node, Label } from 'cc';
import { IGameServerInfo } from '../../shared/gateClient/GameServer';
const { ccclass, property } = _decorator;

@ccclass('ServerInfo')
export class ServerListItem extends Component {
    public serverInfo?: IGameServerInfo;

    public onClick?:(sv:IGameServerInfo)=>void;

    start() {
        var textComp = this.getComponent(Label);
        if (textComp) {
            textComp.string = `${this.serverInfo?.serverName}  (${this.serverInfo?.clientCount}人在线)`;
        }
    }

    onclick(){
        this.onClick?.call(this, this.serverInfo!);
    }
}