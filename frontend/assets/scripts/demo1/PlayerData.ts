
export class PlayerData {
    public connId = "";
    public userName = "";

    public x = 0;
    public z = 0;
    /**移动方向的X轴,用-1,0,1表示, X和Z都为0时表示没有移动*/
    public moveDirX: -1 | 0 | 1 = 0;
    /**移动方向的Z轴(朝下的数学Y轴,或者网页的Y轴),用-1,0,1表示, X和Z都为0时表示没有移动*/
    public moveDirZ: -1 | 0 | 1 = 0;
    /**方向于X轴构成锐角的弧度,计算公式为: Math.Atan(Math.Abs(Y/X)),X为0时,传0*/
    public moveAcuteRad = 0;
    /**每秒移动的距离*/
    public speed = 200;
}
