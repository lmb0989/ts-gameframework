export const PHY_GROUP = {
    Area: 1 << 2,
    Troop: 1 << 3,
    Select: 1 << 4,
};