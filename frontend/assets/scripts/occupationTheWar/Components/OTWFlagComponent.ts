
import { __private, Component, _decorator, Node, Prefab, instantiate, PhysicsSystem, Vec3, UICoordinateTrackerComponent, EventHandler, EventTouch, SpriteComponent, Sprite, SpriteFrame, Camera, Label } from "cc";
import { IArea, ICountry, IMap, IPlayer } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { PositionMapToUIComponent } from "../../common/ui/PositionMapToUIComponent";
import { OTWGameData } from "../Data/OTWGameData";
import { OTWObjectComponent } from "./OTWObjectComponent";
const { ccclass, property } = _decorator;

@ccclass('OTWFlagComponent')
export class OTWFlagComponent extends OTWObjectComponent {
    objType = () => "Flag";
    @property({ type: Label, tooltip: "物体名字显示" })
    public NameText!: Label;
    @property({ type: Label, tooltip: "附加信息" })
    public AppendInfo!: Label;

    country: ICountry | null = null;


    public updateShow() {
        var player = this.country && this.country.playerConnId
            ? this.getGameData()?.map?.allPlayer[this.country.playerConnId] : null;
        if (player) {
            //有玩家,则输出玩家名
            this.NameText.string = player.userName;
        } else if (this.country) {
            //没玩家,则显示国家名
            this.NameText.string = this.country.name;
        } else {
            //也没国家
            this.NameText.string = "无主之地";
        }
    }


    /**
     * 创建并返回UIFlag节点,要自行添加到UI节点
     * @date 2022/3/18 - 下午3:47:29
     *
     * @public
     * @static
     * @template FlagComp extends OTWFlagComponent
     * @param {__private._types_globals__Constructor<FlagComp>} FlagComp
     * @param {() => OTWGameData} getGameData
     * @param {Prefab} flagPrefab
     * @param {Node} gameObjNode
     * @param {Camera} mainCamera
     * @param {Node} gameMgrNode
     * @param {string} handlerFnName
     * @param {string} customEventData
     * @param {(flagComp:FlagComp)=>void} init
     * @returns {Node}
     */
    public static createFlagNode<FlagComp extends OTWFlagComponent>(FlagComp: __private._types_globals__Constructor<FlagComp>,
        getGameData: () => OTWGameData, flagPrefab: Prefab, gameObjNode: Node, mainCamera: Camera,
        country: ICountry | null): Node {

        var flag = instantiate(flagPrefab);

        OTWFlagComponent.initFlagNode<FlagComp>(FlagComp, flag, getGameData, flagPrefab, gameObjNode, mainCamera, country);

        return flag;
    }

    /**
     * 根据已有节点去初始化设置节点组件和数据
     * @date 2022/3/31 - 下午11:43:44
     *
     * @public
     * @static
     * @template FlagComp extends OTWFlagComponent
     * @param {__private._types_globals__Constructor<FlagComp>} FlagComp
     * @param {Node} flag
     * @param {() => OTWGameData} getGameData
     * @param {Prefab} flagPrefab
     * @param {Node} gameObjNode
     * @param {Camera} mainCamera
     * @param {(ICountry | null)} country
     */
    public static initFlagNode<FlagComp extends OTWFlagComponent>(FlagComp: __private._types_globals__Constructor<FlagComp>,
        flag: Node, getGameData: () => OTWGameData, flagPrefab: Prefab, gameObjNode: Node, mainCamera: Camera,
        country: ICountry | null): void {

        flag.name = gameObjNode.name;
        var flagComp = flag.getComponent(FlagComp)!;
        flagComp.getGameData = getGameData;
        flagComp.country = country;

        var c = gameObjNode.getComponent(PositionMapToUIComponent);
        if (!c) c = gameObjNode.addComponent(PositionMapToUIComponent);
        c.targetUI = flag;
        c.camera = mainCamera;
        c.autoMap = true;

        /*
        var c = gameObjNode.addComponent(UICoordinateTrackerComponent);
        c.target = flag;
        c.camera = mainCamera;
        var ev = new EventHandler();
        ev.target = flagComp.node;
        ev.component = "OTWFlagComponent";
        ev.handler = handlerFnName;
        ev.customEventData = customEventData;
        c.syncEvents.push(ev);
        */
    }


}