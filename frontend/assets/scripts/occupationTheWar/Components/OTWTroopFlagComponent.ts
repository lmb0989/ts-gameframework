
import { Component, _decorator, Node, Prefab, instantiate, PhysicsSystem, Vec3, UICoordinateTrackerComponent, EventHandler, EventTouch, SpriteComponent, Sprite, SpriteFrame, Camera, Color } from "cc";
import { IArea, ICountry, IMap, IPlayer, ITroop } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWGameData } from "../Data/OTWGameData";
import { OTWTroopData } from "../Data/OTWTroopData";
import { OTWFlagComponent } from "./OTWFlagComponent";
const { ccclass, property } = _decorator;

@ccclass('OTWTroopFlagComponent')
export class OTWTroopFlagComponent extends OTWFlagComponent {

    troopData: OTWTroopData | null = null;

    onDestroy() {
        this.troopData = null;
    }

    public updateShow() {
        if (!this.troopData) return;
        super.updateShow();
        this.AppendInfo.string = `${this.troopData.troop.troopsCount}`;

        if (this.troopData.country) {
            if (this.getGameData().myCountryId == this.troopData.country.countryId) {
                if (!this.NameText.color.equals(Color.RED)) {
                    this.NameText.color.set(Color.RED);
                    this.NameText.updateRenderData(true);
                }
            } else {
                if (!this.NameText.color.equals(Color.WHITE)) {
                    this.NameText.color.set(Color.WHITE);
                    this.NameText.updateRenderData(true);
                }
            }
        }
    }

}