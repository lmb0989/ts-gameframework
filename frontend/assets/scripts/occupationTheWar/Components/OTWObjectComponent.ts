
import { Component, _decorator, Node, Prefab, instantiate, PhysicsSystem, Vec3, UICoordinateTrackerComponent, EventHandler, EventTouch, SpriteComponent, Sprite, SpriteFrame, Camera, Label, ITriggerEvent } from "cc";
import { IArea, ICountry, IMap, IPlayer } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWGameData } from "../Data/OTWGameData";
const { ccclass, property } = _decorator;

export class OTWObjectComponent extends Component {
    getGameData!: () => OTWGameData;
    objType?:()=>string;
    
    /**
     * 更新逻辑数据
     * @date 2022/3/18 - 下午5:34:24
     *
     * @abstract
     * @param {number} dt
     */
    updateData?(dt:number):void;
}