
import { Component, _decorator, Node, Prefab, instantiate, PhysicsSystem, Vec3, UICoordinateTrackerComponent, EventHandler, EventTouch, SpriteComponent, Sprite, SpriteFrame, Camera, Color } from "cc";
import { IArea, ICountry, IMap, IPlayer } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWAreaData } from "../Data/OTWAreaData";
import { OTWGameData } from "../Data/OTWGameData";
import { OTWFlagComponent } from "./OTWFlagComponent";
const { ccclass, property } = _decorator;

@ccclass('OTWAreaFlagComponent')
export class OTWAreaFlagComponent extends OTWFlagComponent {
    areaInfo: OTWAreaData | null = null;

    onDestroy() {
        this.areaInfo = null;
    }

    start() {
        if (!this.areaInfo) return;

    }

    public updateShow() {
        if (!this.areaInfo) return;
        super.updateShow();
        var area = this.getGameData().map?.allArea[this.areaInfo.area.areaIndex];
        if (area) {
            this.AppendInfo.string = `${area.troopsCurr}/${area.troopsMax}`;
        }

        if (this.areaInfo.country) {
            if (this.getGameData().myCountryId == this.areaInfo.country.countryId) {
                if (!this.NameText.color.equals(Color.RED)) {
                    this.NameText.color.set(Color.RED);
                    this.NameText.updateRenderData(true);
                }
            } else {
                if (!this.NameText.color.equals(Color.WHITE)) {
                    this.NameText.color.set(Color.WHITE);
                    this.NameText.updateRenderData(true);
                }
            }
        }
    }
}