
import { _decorator, Node } from "cc";
import { IArea, ICountry } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWAreaComponent } from "../Components/OTWAreaComponent";
import { OTWAreaFlagComponent } from "../Components/OTWAreaFlagComponent";

export interface OTWAreaData {
    area: IArea;
    country: ICountry | null;
    areaNode: Node;
    areaComp: OTWAreaComponent;
    flagNode: Node;
    flagComp: OTWAreaFlagComponent;
}