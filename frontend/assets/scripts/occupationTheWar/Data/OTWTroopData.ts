
import { _decorator, Node } from "cc";
import { IArea, ICountry, ITroop } from "../../../shared/gameClient/games/OccupationTheWarModels";
import { OTWTroopComponent } from "../Components/OTWTroopComponent";
import { OTWTroopFlagComponent } from "../Components/OTWTroopFlagComponent";
import { OTWTroopRenderComponent } from "../Components/OTWTroopRenderComponent";

export interface OTWTroopData {
    troop: ITroop;
    fromArea: IArea;
    toArea: IArea;
    country: ICountry | null;
    renderNode: Node;
    renderComp: OTWTroopRenderComponent,
    troopNode: Node;
    troopComp: OTWTroopComponent,
    flagNode: Node;
    flagComp: OTWTroopFlagComponent;
}