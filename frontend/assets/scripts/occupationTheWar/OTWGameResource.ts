import { _decorator, Component, Node, Prefab, Camera, instantiate, Collider, CollisionEventType, ITriggerEvent, Vec3, v3, Quat } from 'cc';
const { ccclass, property } = _decorator;


@ccclass('OTWGameResource')
export class OTWGameResource extends Component {

    @property({ type: Prefab, tooltip: "地块模型预制体" })
    public AreaPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "地块旗帜UI的预制体" })
    public AreaFlagPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "士兵渲染模型预制体" })
    public TroopRenderPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "士兵碰撞体预制体" })
    public TroopColliderPrefab?: Prefab;
    @property({ type: Prefab, tooltip: "士兵旗帜UI的预制体" })
    public TroopFlagPrefab?: Prefab;
}