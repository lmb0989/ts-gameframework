
export class Result<T>{
    public succ = false;
    public err = "";
    public code = 0;
    public data: T | undefined;
    static buildErr<T>(errMsg: string, code = 0): Result<T> {
        const r = new Result<T>();
        r.succ = false;
        r.err = errMsg;
        r.code = code;
        return r;
    }
    static buildSucc<T>(data: T): Result<T> {
        const r = new Result<T>();
        r.succ = true;
        r.data = data;
        return r;
    }
}
