
/**需要连接后立即发出请求,否则超时将被断开连接 */
export interface ReqReconnect {
    /**之前连接上的连接ID */
    connectionId:string;
}

export interface ResReconnect {
    /**新的连接ID, 可用于下次断线重连*/
    connectionId:string;
}