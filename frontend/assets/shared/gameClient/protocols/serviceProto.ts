import { ServiceProto } from 'tsrpc-proto';
import { MsgAfterFrames } from './MsgAfterFrames';
import { MsgDisconnect } from './MsgDisconnect';
import { MsgInpFrame } from './MsgInpFrame';
import { MsgRequireSyncState } from './MsgRequireSyncState';
import { MsgSyncFrame } from './MsgSyncFrame';
import { MsgSyncState } from './MsgSyncState';
import { ReqLogin, ResLogin } from './PtlLogin';
import { ReqReconnect, ResReconnect } from './PtlReconnect';

export interface ServiceType {
    api: {
        "Login": {
            req: ReqLogin,
            res: ResLogin
        },
        "Reconnect": {
            req: ReqReconnect,
            res: ResReconnect
        }
    },
    msg: {
        "AfterFrames": MsgAfterFrames,
        "Disconnect": MsgDisconnect,
        "InpFrame": MsgInpFrame,
        "RequireSyncState": MsgRequireSyncState,
        "SyncFrame": MsgSyncFrame,
        "SyncState": MsgSyncState
    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 15,
    "services": [
        {
            "id": 10,
            "name": "AfterFrames",
            "type": "msg"
        },
        {
            "id": 13,
            "name": "Disconnect",
            "type": "msg"
        },
        {
            "id": 7,
            "name": "InpFrame",
            "type": "msg"
        },
        {
            "id": 11,
            "name": "RequireSyncState",
            "type": "msg"
        },
        {
            "id": 8,
            "name": "SyncFrame",
            "type": "msg"
        },
        {
            "id": 12,
            "name": "SyncState",
            "type": "msg"
        },
        {
            "id": 5,
            "name": "Login",
            "type": "api"
        },
        {
            "id": 6,
            "name": "Reconnect",
            "type": "api"
        }
    ],
    "types": {
        "MsgAfterFrames/MsgAfterFrames": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "stateData",
                    "type": {
                        "type": "Any"
                    }
                },
                {
                    "id": 1,
                    "name": "stateFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "afterFrames",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../GameSyncFrame/GameSyncFrame"
                        }
                    }
                },
                {
                    "id": 3,
                    "name": "maxSyncFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "serverSyncFrameRate",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "../GameSyncFrame/GameSyncFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "connectionInputs",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../GameSyncFrame/ConnectionInputFrame"
                        }
                    }
                }
            ],
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "../GameSyncFrame/ConnectionInputFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "connectionId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "operates",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../GameSyncFrame/ConnectionInputOperate"
                        }
                    }
                }
            ],
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "../GameSyncFrame/ConnectionInputOperate": {
            "type": "Interface",
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "MsgDisconnect/MsgDisconnect": {
            "type": "Interface"
        },
        "MsgInpFrame/MsgInpFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "operates",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../GameSyncFrame/ConnectionInputOperate"
                        }
                    }
                }
            ],
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "MsgRequireSyncState/MsgRequireSyncState": {
            "type": "Interface"
        },
        "MsgSyncFrame/MsgSyncFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "syncFrame",
                    "type": {
                        "type": "Reference",
                        "target": "../GameSyncFrame/GameSyncFrame"
                    }
                },
                {
                    "id": 1,
                    "name": "frameIndex",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgSyncState/MsgSyncState": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "stateData",
                    "type": {
                        "type": "Any"
                    }
                },
                {
                    "id": 1,
                    "name": "stateFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlLogin/ReqLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "loginToken",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlLogin/ResLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "connectionId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlReconnect/ReqReconnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "connectionId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlReconnect/ResReconnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "connectionId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        }
    }
};