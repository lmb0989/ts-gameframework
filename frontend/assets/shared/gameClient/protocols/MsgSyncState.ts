export interface MsgSyncState {
    /**状态的数据*/
    stateData: any;
    /**状态所在帧索引*/
    stateFrameIndex: number;
}

// export const conf = {}