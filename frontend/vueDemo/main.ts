import { createApp } from 'vue';
import "./assets/css/iconfont.css";
import "./assets/css/Joystick.css";
import App from './App.vue';

import "../assets/env";

const app = createApp(App);
app.mount('#app');
