import { assert } from 'chai';
//import { describe, before, it } from 'mocha';
import { FrameSyncExecutor, IFrameSyncConnect } from "../../assets/scripts/common/FrameSyncExecutor";
import { ConnectionInputOperate } from '../../assets/shared/gameClient/GameSyncFrame';



describe('FrameSyncExecutor', function () {

    let testConnect: IFrameSyncConnect = {
        onAfterFrames: () => { },
        onSyncFrame: () => { },
        onRequireSyncState: () => { },
        sendSyncState: function (msg): void {
            console.log('sendSyncState', msg);
        },
        sendInpFrame: function (msg): void {
            console.log('sendInpFrame', msg);
        },
        disconnect: function (): void {
            console.log('disconnect');
        }
    };

    it('syncOneFrame 1', async function () {
        let lastStateData = {
            allPlayers: {} as { [connId: string]: string }
        };
        let frameSyncExecutor = new FrameSyncExecutor(testConnect, {
            execInput_NewPlayer: (connId: string, inputFrame: ConnectionInputOperate) => {
                lastStateData.allPlayers[connId] = inputFrame.userName;
            },
            execInput_RemovePlayer: (connId: string, inputFrame: ConnectionInputOperate) => {
                delete lastStateData.allPlayers[connId];
            },
        }, (stateData, stateFrameIndex) => {
            console.log('syncStateData:', stateData, stateFrameIndex);
        }, (dt, frameIndex) => {
            console.log('execOneFrame:', frameIndex);
        }, () => {
            console.log('getStateData:', lastStateData);
            return lastStateData
        });
        //模拟启动自动执行帧
        frameSyncExecutor.executeFrameStop = false;

        var v = frameSyncExecutor.afterFrames.length;
        assert.ok(v === 0, "应为0,实际为" + v);
        v = frameSyncExecutor.stateFrameIndex;
        assert.ok(v === -1, "应为-1,实际为" + v);
        v = frameSyncExecutor.executeFrameIndex;
        assert.ok(v === -1, "应为-1,实际为" + v);
        v = frameSyncExecutor.maxCanRenderFrameIndex;
        assert.ok(v === -1, "应为-1,实际为" + v);

        //追帧, 到1, 并且没后续
        testConnect.onAfterFrames({
            stateData: lastStateData,
            stateFrameIndex: 1,
            afterFrames: [{
                connectionInputs:[],
            },{
                connectionInputs: [{
                    connectionId: "1",
                    operates: [{
                        inputType: "NewPlayer",
                        userName: "a"
                    }],
                }],
            }],
            maxSyncFrameIndex: 3,
            serverSyncFrameRate: 60,
        });

        v = frameSyncExecutor.afterFrames.length;
        assert.ok(v === 2, "应为2,实际为" + v);
        v = frameSyncExecutor.stateFrameIndex;
        assert.ok(v === 1, "应为1,实际为" + v);
        v = frameSyncExecutor.executeFrameIndex;
        assert.ok(v === 1, "应为1,实际为" + v);
        v = frameSyncExecutor.maxCanRenderFrameIndex;
        assert.ok(v === 3, "应为1,实际为" + v);

        //模拟接收到一个同步帧:新玩家
        testConnect.onSyncFrame({
            frameIndex: 4,
            syncFrame: {
                connectionInputs:[],
            }
        });

        v = frameSyncExecutor.afterFrames.length;
        assert.ok(v === 3, "应为3,实际为" + v);
        v = frameSyncExecutor.stateFrameIndex;
        assert.ok(v === 1, "应为1,实际为" + v);
        v = frameSyncExecutor.executeFrameIndex;
        assert.ok(v === 1, "应为1,实际为" + v);
        v = frameSyncExecutor.maxCanRenderFrameIndex;
        assert.ok(v === 4, "应为4,实际为" + v);

        var frameEnd = frameSyncExecutor.executeOneFrame(0.012);
        assert.ok(frameEnd === false, "应为false,实际为" + frameEnd);
        v = frameSyncExecutor.afterFrames.length;
        assert.ok(v === 3, "应为3,实际为" + v);
        v = frameSyncExecutor.stateFrameIndex;
        assert.ok(v === 1, "应为1,实际为" + v);
        v = frameSyncExecutor.executeFrameIndex;
        assert.ok(v === 2, "应为2,实际为" + v);
        v = frameSyncExecutor.maxCanRenderFrameIndex;
        assert.ok(v === 4, "应为4,实际为" + v);

    });
})