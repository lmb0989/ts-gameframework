
import { assert } from 'chai';
import { HttpClient } from 'tsrpc';
import { GameServerInfo } from '../../src/shared/gateClient/GameServer';
import { GateClient } from '../../src/shared/gateClient/GateClient';

function getGateClient(gateServerUrl: string): GateClient {
    return new GateClient((proto, opt) => {
        return new HttpClient(proto, opt);
    }, gateServerUrl);
};


describe('ApiGetServerList', function () {
    let client = getGateClient('http://127.0.0.1:7100');

    it('ApiGetServerList get list', async function () {
        let ret1 = await client.getGameServers();
        assert.ok(ret1.isSucc, ret1.err?.message);
    });


    /* 需要游戏服务器启动的,就不放在gate单元测试中了
    it('loginByCustomUserName', async function () {

        let svRet = await client.getGameServers();
        assert.ok(svRet.isSucc, svRet.err?.message);
        assert.ok(svRet.res?.GameServerList?.length, "没有可用连接的游戏服务器");
        var gameSv = svRet.res?.GameServerList[0] as GameServerInfo;
        let ret1 = await client.loginByCustomUserName(gameSv, "testUser");
        assert.ok(ret1.succ, ret1.err);
    });*/

})