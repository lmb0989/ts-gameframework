import { BaseConnection, WsServerOptions } from "tsrpc";
import { getGateConfig } from "./gateConfigMgr";
import { IGameServerInfo, GameServerCluster } from "./shared/gateClient/GameServer";
import { logger } from "./shared/tsgf/logger";
import { ClusterMgr, IClusterNodeCfg, IClusterNodeInfo } from "./shared/tsgfServer/cluster/ClusterMgr";
import { ServiceType as ClusterServiceType, serviceProto as clusterServiceProto } from "./shared/tsgfServer/cluster/protocols/serviceProto";


/**游戏服务器管理节点，依赖redis*/
export class GameServerMgr extends ClusterMgr<IGameServerInfo>{
    constructor(getNodesCfg: () => IClusterNodeCfg[], serverOption: Partial<WsServerOptions<ClusterServiceType>>) {
        super("GameServer", getNodesCfg, serverOption);
        
    }

    /**
     * 从redis中获取所有游戏服务器列表, 分布式时，gate服务器和游戏服务器管理节点，可能不是同个实例，所以使用本方法来获取
     * @date 2022/4/20 - 16:48:25
     *
     * @public
     * @static
     * @async
     * @template NodeInfo
     * @param {string} clusterTypeKey 集群类型标识，用在各种场合进行区分的。需要和构造ClussterMgr时的值一致
     * @returns {Promise<IClusterNodeInfo<IGameServerInfo>[]>}
     */
     public static async getServersFromRedis(): Promise<IClusterNodeInfo<IGameServerInfo>[]> {
        var list = await ClusterMgr.getNodeInfosFromRedis<IGameServerInfo>("GameServer");
        return list;
     }
}