
import { IGameServerInfo } from "../GameServer";


/**
 * 获取当前可用的服务器列表
 */
export interface ReqGetServerList { 
    
}

export interface ResGetServerList {
    /** 游戏服务器列表 */
    GameServerList: IGameServerInfo[]
}