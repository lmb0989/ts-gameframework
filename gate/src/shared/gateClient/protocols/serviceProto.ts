import { ServiceProto } from 'tsrpc-proto';
import { ReqGetServerList, ResGetServerList } from './PtlGetServerList';
import { ReqLoginToGame, ResLoginToGame } from './PtlLoginToGame';

export interface ServiceType {
    api: {
        "GetServerList": {
            req: ReqGetServerList,
            res: ResGetServerList
        },
        "LoginToGame": {
            req: ReqLoginToGame,
            res: ResLoginToGame
        }
    },
    msg: {

    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 7,
    "services": [
        {
            "id": 2,
            "name": "GetServerList",
            "type": "api"
        },
        {
            "id": 3,
            "name": "LoginToGame",
            "type": "api"
        }
    ],
    "types": {
        "PtlGetServerList/ReqGetServerList": {
            "type": "Interface"
        },
        "PtlGetServerList/ResGetServerList": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "GameServerList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../GameServer/IGameServerInfo"
                        }
                    }
                }
            ]
        },
        "../GameServer/IGameServerInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "serverId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "serverName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "serverWSUrl",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 3,
                    "name": "clientCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "extendData",
                    "type": {
                        "type": "Any"
                    },
                    "optional": true
                }
            ]
        },
        "PtlLoginToGame/ReqLoginToGame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "gameServerId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlLoginToGame/ResLoginToGame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "gameLoginToken",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        }
    }
};