import { GameServerCluster } from "./shared/gateClient/GameServer";
import path from "path";
import { getConfig, startWatchConfig } from "./shared/tsgfServer/gfConfigMgr";
import { RedisConfig } from "./shared/tsgfServer/redisHelper";

var gateConfigPath = path.resolve(__dirname, '../gf.gate.config.json');
export interface IServerCfg{
    port:number;
}
export interface GfGateConfig {
    /**gate服务配置*/
    gateServer: IServerCfg;
    /**游戏集群服务配置*/
    gameClusterServer: IServerCfg;
    /**配置所有可以连接集群的游戏服务器节点 */
    gameServerCfgList: GameServerCluster[];
    /**匹配集群服务配置*/
    matchClusterServer: IServerCfg;
    /**配置所有可以连接集群的匹配服务器节点 */
    matchServerCfgList: GameServerCluster[];
    /**redis配置 */
    redisConfig:RedisConfig;
}

/**开始监控gate服务器配置,并返回获取到的配置对象 */
export function startWatchGateConfig():GfGateConfig{
    var ret = startWatchConfig(gateConfigPath);
    if (!ret.succ) {
        console.error(ret.err);
    }
    return ret.data as GfGateConfig;
}

/**获取缓存中的配置，推荐每次使用配置都调用本方法获取最新*/
export function getGateConfig(): GfGateConfig {
    var ret = getConfig(gateConfigPath);
    if (!ret.succ) {
        console.error(ret.err);
    }
    return ret.data as GfGateConfig;
}