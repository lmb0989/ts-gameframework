# Details

Date : 2022-04-04 22:08:10

Directory d:\Dev\JS\ts-gameframework\

Total : 31 files,  10293 codes, 164 comments, 98 blanks, all 10555 lines

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Dockerfile](/Dockerfile) | Docker | 15 | 7 | 8 | 30 |
| [deploy/install_runasAdmin.cmd](/deploy/install_runasAdmin.cmd) | Batch | 5 | 0 | 0 | 5 |
| [deploy/uninstall_runasAdmin.cmd](/deploy/uninstall_runasAdmin.cmd) | Batch | 4 | 0 | 0 | 4 |
| [gf.gate.config.json](/gf.gate.config.json) | JSON | 16 | 0 | 0 | 16 |
| [jest.config.js](/jest.config.js) | JavaScript | 10 | 0 | 0 | 10 |
| [package-lock.json](/package-lock.json) | JSON | 9,462 | 0 | 1 | 9,463 |
| [package.json](/package.json) | JSON | 26 | 0 | 0 | 26 |
| [src/api/gameCluster/ApiJoinCluster.ts](/src/api/gameCluster/ApiJoinCluster.ts) | TypeScript | 13 | 0 | 3 | 16 |
| [src/api/gateClient/ApiGetServerList.ts](/src/api/gateClient/ApiGetServerList.ts) | TypeScript | 8 | 0 | 2 | 10 |
| [src/api/gateClient/ApiLoginToGame.ts](/src/api/gateClient/ApiLoginToGame.ts) | TypeScript | 15 | 0 | 1 | 16 |
| [src/gameServerMgr.ts](/src/gameServerMgr.ts) | TypeScript | 40 | 1 | 4 | 45 |
| [src/gateConfigMgr.ts](/src/gateConfigMgr.ts) | TypeScript | 23 | 3 | 3 | 29 |
| [src/index.ts](/src/index.ts) | TypeScript | 39 | 0 | 8 | 47 |
| [src/shared/gameCluster/gfConfigMgr.ts](/src/shared/gameCluster/gfConfigMgr.ts) | TypeScript | 52 | 22 | 10 | 84 |
| [src/shared/gameCluster/protocols/MsgSyncServerInfo.ts](/src/shared/gameCluster/protocols/MsgSyncServerInfo.ts) | TypeScript | 4 | 1 | 2 | 7 |
| [src/shared/gameCluster/protocols/PtlJoinCluster.ts](/src/shared/gameCluster/protocols/PtlJoinCluster.ts) | TypeScript | 7 | 7 | 3 | 17 |
| [src/shared/gameCluster/protocols/serviceProto.ts](/src/shared/gameCluster/protocols/serviceProto.ts) | TypeScript | 108 | 0 | 2 | 110 |
| [src/shared/gameCluster/redisHelper.ts](/src/shared/gameCluster/redisHelper.ts) | TypeScript | 84 | 50 | 8 | 142 |
| [src/shared/gateClient/GameServer.ts](/src/shared/gateClient/GameServer.ts) | TypeScript | 18 | 11 | 3 | 32 |
| [src/shared/gateClient/GateClient.ts](/src/shared/gateClient/GateClient.ts) | TypeScript | 36 | 17 | 9 | 62 |
| [src/shared/gateClient/UserInfo.ts](/src/shared/gateClient/UserInfo.ts) | TypeScript | 4 | 0 | 1 | 5 |
| [src/shared/gateClient/protocols/PtlGetServerList.ts](/src/shared/gateClient/protocols/PtlGetServerList.ts) | TypeScript | 6 | 4 | 5 | 15 |
| [src/shared/gateClient/protocols/PtlLoginToGame.ts](/src/shared/gateClient/protocols/PtlLoginToGame.ts) | TypeScript | 8 | 6 | 5 | 19 |
| [src/shared/gateClient/protocols/serviceProto.ts](/src/shared/gateClient/protocols/serviceProto.ts) | TypeScript | 125 | 0 | 3 | 128 |
| [src/shared/tsgf/AClient.ts](/src/shared/tsgf/AClient.ts) | TypeScript | 16 | 8 | 3 | 27 |
| [src/shared/tsgf/Result.ts](/src/shared/tsgf/Result.ts) | TypeScript | 19 | 0 | 2 | 21 |
| [src/shared/tsgf/Utils.ts](/src/shared/tsgf/Utils.ts) | TypeScript | 7 | 8 | 2 | 17 |
| [src/shared/tsgf/logger.ts](/src/shared/tsgf/logger.ts) | TypeScript | 28 | 3 | 1 | 32 |
| [test/api/data.test.ts](/test/api/data.test.ts) | TypeScript | 16 | 10 | 8 | 34 |
| [tsconfig.json](/tsconfig.json) | JSON with Comments | 19 | 0 | 0 | 19 |
| [tsrpc.config.ts](/tsrpc.config.ts) | TypeScript | 60 | 6 | 1 | 67 |

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)