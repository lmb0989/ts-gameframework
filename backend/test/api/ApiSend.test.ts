

import { assert } from 'chai';
import { HttpClient, TsrpcError, WsClient } from 'tsrpc';
import { GameClient } from '../../src/shared/gameClient/GameClient';
import { GameServerInfo } from '../../src/shared/gateClient/GameServer';
import { GateClient } from '../../src/shared/gateClient/GateClient';

function getGameClient(serverUrl: string): GameClient {
    return new GameClient((proto, opt) => {
        return new WsClient(proto, opt);
    }, serverUrl);
}
function getGateClient(gateServerUrl: string): GateClient {
    return new GateClient((proto, opt) => {
        return new HttpClient(proto, opt);
    }, gateServerUrl);
};


test('ApiLogin',async function () {
    let gateClient: GateClient;
    let gameServerInfo: GameServerInfo;
    let gameClient: GameClient;

    gateClient = getGateClient("http://127.0.0.1:7100/");
    let svRet = await gateClient.getGameServers();
    assert.ok(svRet.isSucc, svRet.err?.message);
    assert.ok(svRet.res?.GameServerList?.length, "没有可用连接的游戏服务器");
    gameServerInfo = svRet.res?.GameServerList[0] as GameServerInfo;
    gameClient = getGameClient(gameServerInfo.serverWSUrl);

    var ret = await gateClient.loginByCustomUserName(gameServerInfo, "testUser");
    assert.ok(ret.succ, ret.err)

    await gameClient.client.disconnect();
})