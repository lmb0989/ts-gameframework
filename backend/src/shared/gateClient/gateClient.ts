
import { BaseHttpClient, BaseHttpClientOptions, BaseWsClient, BaseWsClientOptions } from "tsrpc-base-client";
import { ApiReturn, ServiceProto } from "tsrpc-proto";

import { serviceProto as gateClientProto, ServiceType as gateServiceType } from "./protocols/serviceProto";
import { IGameServerInfo } from "./GameServer";
import { Result } from "../tsgf/Result";
import { AHttpClient } from "../tsgf/AClient";
import { ResGetServerList } from "./protocols/PtlGetServerList";


export class GateClient extends AHttpClient<gateServiceType>{
    constructor(buildClient: (proto: ServiceProto<gateServiceType>, options?: Partial<BaseHttpClientOptions>) => BaseHttpClient<gateServiceType>,
        serverUrl: string) {
        super(buildClient, gateClientProto, {
            server: serverUrl,
            json: false,
            logger: console,
        })
    }

    /**
     * 获取游戏服务器列表
     * @returns game servers 
     */
    public async getGameServers(): Promise<ApiReturn<ResGetServerList>> {
        return await this.client.callApi(
            "GetServerList",
            {}
        );
    }

    /**
     * 使用自定义用户名登录
     * @param gameServerInfo 
     * @param userName 
     * @returns 返回是否有错误消息,null表示成功
     */
    public async loginByCustomUserName(gameServerInfo: IGameServerInfo, userName: string): Promise<Result<string>> {
        const ret = await this.client.callApi("LoginToGame", {
            gameServerId: gameServerInfo.serverId as string,
            userName: userName,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message);
        }
        return Result.buildSucc(ret.res.gameLoginToken);
    }
}


declare global {
    
    /**
     * [需要实际客户端实现的函数]获取入口客户端
     * @date 2022/2/18 - 下午11:31:49
     *
     * @param {string} gateServerUrl
     * @returns {GateClient}
     */
    function getGateClient(gateServerUrl: string): GateClient;
}