import { GameSyncFrame } from "../GameSyncFrame";


/**服务端广播给所有客户端的每帧数据*/
export interface MsgSyncFrame {
    /**要同步的游戏帧数据*/
    syncFrame:GameSyncFrame;
    /**游戏帧所属索引*/
    frameIndex:number;
}

