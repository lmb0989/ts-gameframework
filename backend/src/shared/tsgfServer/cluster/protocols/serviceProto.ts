import { ServiceProto } from 'tsrpc-proto';
import { MsgAssignTask } from './MsgAssignTask';
import { MsgClusterSyncNodeInfo } from './MsgClusterSyncNodeInfo';
import { ReqClusterLogin, ResClusterLogin } from './PtlClusterLogin';

export interface ServiceType {
    api: {
        "ClusterLogin": {
            req: ReqClusterLogin,
            res: ResClusterLogin
        }
    },
    msg: {
        "AssignTask": MsgAssignTask,
        "ClusterSyncNodeInfo": MsgClusterSyncNodeInfo
    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 3,
    "services": [
        {
            "id": 3,
            "name": "AssignTask",
            "type": "msg"
        },
        {
            "id": 0,
            "name": "ClusterSyncNodeInfo",
            "type": "msg"
        },
        {
            "id": 1,
            "name": "ClusterLogin",
            "type": "api"
        }
    ],
    "types": {
        "MsgAssignTask/MsgAssignTask": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "taskId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "taskInfo",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "MsgClusterSyncNodeInfo/MsgClusterSyncNodeInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "nodeInfo",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "PtlClusterLogin/ReqClusterLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "nodeId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "clusterKey",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "nodeInfo",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "PtlClusterLogin/ResClusterLogin": {
            "type": "Interface"
        }
    }
};