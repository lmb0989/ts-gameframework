import { gameConnMgr } from "../server";
import { ReqReconnect, ResReconnect } from "../shared/gameClient/protocols/PtlReconnect";
import { GameApiCall } from "./base";
import { apiErrorThenClose } from "../shared/tsgfServer/ApiBase";

export async function ApiReconnect(call: GameApiCall<ReqReconnect, ResReconnect>) {
    var err = gameConnMgr.connReconnect(call.conn, call.req.connectionId);
    if (err) {
        return apiErrorThenClose(call, err, { code: 5001 });
    }
    call.succ({
        connectionId: call.conn.connectionId,
    });
}