import { getGameConfig } from "../gameConfigMgr";
import { gameConnMgr } from "../server";
import { ReqLogin, ResLogin } from "../shared/gameClient/protocols/PtlLogin";
import { checkGameLoginToken } from "../shared/tsgfServer/gfConfigMgr";
import { GameApiCall } from "./base";
import { apiErrorThenClose } from "../shared/tsgfServer/ApiBase";

export async function ApiLogin(call: GameApiCall<ReqLogin, ResLogin>) {
    var sCfg = getGameConfig();
    var userInfo = await checkGameLoginToken(sCfg.gameServer.serverId, call.req.loginToken);
    if (!userInfo) {
        return apiErrorThenClose(call, "错误的token!");
    }
    var err = gameConnMgr.checkConnAuthorizing(call.conn, userInfo);
    if (err) {
        return apiErrorThenClose(call, err);
    }
    gameConnMgr.setConnAuthorized(call.conn, userInfo);
    call.succ({
        connectionId: call.conn.connectionId,
    });
}